

Pod::Spec.new do |spec|

  spec.name         = "2dinCoreBaseLib"
  spec.version      = "0.0.1"
  spec.summary      = "A CocoaPods library written in Swift"
  spec.description  = <<-DESC
			Library use for base in VIPER Pattern
                   DESC
  spec.homepage     = "https://gitlab.com/mask.joker.bad/ios-core-base"
  spec.license      = { :type => "MIT" }
  spec.author             = { "decky" => "mask.joker.bad@gmail.com" }
  spec.ios.deployment_target = "11.0"
  spec.swift_version = "4.0"
  spec.source       = { :git => "https://gitlab.com/mask.joker.bad/ios-core-base.git", :tag => "#{spec.version}" }
  spec.source_files  = "CoreBase/**/*.{h,m,swift}"

end
