//
//  BaseRouter.swift
//  CoreBase
//
//  Created by Dev Guest on 27/06/23.
//

import Foundation


open class BaseRouter {
 
    required public init() {}
    
    open func createModule(data: [String:Any]? = nil) -> BaseViewController {
        return BaseViewController()
    }
}
