//
//  BaseViewController.swift
//  CoreBase
//
//  Created by Dev Guest on 27/06/23.
//

import Foundation
import UIKit
protocol BaseView {
    func setupHierarchy()
    func setupConstraint()
    func setupStyle()
    func setupAction()
}

open class BaseViewController: UIViewController{
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        setupHierarchy()
        setupConstraint()
        setupStyle()
        setupAction()
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    var viewController : BaseViewController?
    
    
}

extension BaseViewController: BaseView{
    
    @objc open func setupHierarchy() {}
    
    @objc open func setupConstraint() {}
    
    @objc open func setupStyle() {
        self.view.backgroundColor = .white
    }
    
    @objc open func setupAction() {}
}
